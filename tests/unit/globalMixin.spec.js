import { expect } from 'chai';
import globalMixin from '@/globalMixin';

describe('globalMixin.js', () => {
  it('check globalMixin config', () => {
    expect(globalMixin.data()).to.be.an('object');
    expect(globalMixin.data().config.lotterySimulator).to.be.an('object');
    expect(globalMixin.data().config.lotterySimulator.numberOfNumbers).to.be.a('number');
    expect(globalMixin.data().config.lotterySimulator.number.minValue).to.be.a('number');
    expect(globalMixin.data().config.lotterySimulator.ticketPrice).to.be.a('number');
    expect(globalMixin.data().config.lotterySimulator.oneYearInWeeks).to.be.a('number');
  });

  it('check numberMask()', () => {
    expect(globalMixin.methods.numberMask(1)).to.equal('1');
    expect(globalMixin.methods.numberMask(12)).to.equal('12');
    expect(globalMixin.methods.numberMask(123)).to.equal('123');
    expect(globalMixin.methods.numberMask(1234)).to.equal('1 234');
    expect(globalMixin.methods.numberMask(12345)).to.equal('12 345');
    expect(globalMixin.methods.numberMask(123456)).to.equal('123 456');
    expect(globalMixin.methods.numberMask(1234567)).to.equal('1 234 567');
  });
});
